/*
 * pps_iguana -- pps client for Versalogic Iguana board.
 *
 * This PPS client driver supports monitoring a 1-PPS signal connected
 * to one of the 16 lines on the SPI-bus based digital I/O chip. The
 * I/O lines are split across two 8-bit ports, port-A has line 0-7 and
 * port-B has lines 8-15.
 *
 * Unfortunately, this board also has an A/D converter on the same
 * SPI bus. To avoid bus contention, when we receive a 1-PPS interrupt,
 * we also sample all eight A/D channels and make the data available
 * through a /proc interface.
 *
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <asm/signal.h>
#include <linux/time.h>
#include <linux/slab.h>
#include <linux/pps_kernel.h>
#include <asm/io.h>
#include <asm/system.h>
#include <linux/delay.h>
#include <linux/spinlock.h>
#include <linux/time.h>
#include <linux/printk.h>
#include <linux/proc_fs.h>

#define DRVDESC "Versalogic Iguana PPS client"

/* Versalogic Iguana SPIX register definitions */
#define SPIX_BASE       0xca8
#define SPIX_NREGS      6
#define SPI_CONTROL     0
#define SPI_STATUS      1
#define SPI_DATA0       2
#define SPI_DATA1       3
#define SPI_DATA2       4
#define SPI_DATA3       5

/* bitmasks */
#define SPI_CPOL_HIGH   0x80
#define SPI_CPHA_FALL   0x40
#define SPI_LEN_8       0x00
#define SPI_LEN_16      (0x01 << 4)
#define SPI_LEN_24      (0x02 << 4)
#define SPI_LEN_32      (0x03 << 4)
#define SPI_MAN_SS      0x08
#define SPI_IRQ_3       0x00
#define SPI_IRQ_4       (0x01 << 6)
#define SPI_IRQ_5       (0x02 << 6)
#define SPI_IRQ_10      (0x03 << 6)
#define SPI_CLK_1MHZ    0x00
#define SPI_CLK_2MHZ    (0x01 << 4)
#define SPI_CLK_4MHZ    (0x02 << 4)
#define SPI_CLK_8MHZ    (0x03 << 4)

#define SPI_IRQ_ENABLE      0x08
#define SPI_IRQ_ASSERT      0x02
#define SPI_BUSY            0x01
#define SPI_WRITE_SLAVE     0x40
#define SPI_READ_SLAVE      0x41

#define SPI_DIO_ADDR        0x06
#define SPI_ADC_ADDR        0x05

/* SPI_CONTROL value to access digital i/o chip */
#define SPI_DIO_CONTROL (SPI_LEN_24 | SPI_DIO_ADDR)
/* SPI_STATUS value to access digital i/o chip */
#define SPI_DIO_STATUS SPI_CLK_8MHZ
/* SPI_CONTROL value to access the A/D chip */
#define SPI_ADC_CONTROL (SPI_LEN_16 | SPI_ADC_ADDR)
/* SPI_STATUS value to access the A/D chip */
#define SPI_ADC_STATUS SPI_CLK_2MHZ

/* MCP23X17 I/O chip registers, BANK=0 */
#define IODIRA      0x00
#define IODIRB      0x01
#define IPOLA       0x02
#define IPOLB       0x03
#define GPINTENA    0x04
#define GPINTENB    0x05
#define DEFVALA     0x06
#define DEFVALB     0x07
#define INTCONA     0x08
#define INTCONB     0x09
#define IOCONA      0x0a
#define IOCONB      0x0b /* same as IOCONA */
#define GPPUA       0x0c
#define GPPUB       0x0d
#define INTFA       0x0e
#define INTFB       0x0f
#define INTCAPA     0x10
#define INTCAPB     0x11
#define GPIOA       0x12
#define GPIOB       0x13
#define OLATA       0x14
#define OLATB       0x15

/* ADC macros */
#define ADC_CHANNELS    8
#define ADC_CTL_REG     0xcaf
#define ADC_NREGS       1

/* bitmasks */
#define ADC_CONV        0x01
#define ADC_BUSY        0x04
#define ADC_VRANGE_5PP  0x00
#define ADC_VRANGE_10PP (0x01 << 2)
#define ADC_VRANGE_5P   (0x02 << 2)
#define ADC_VRANGE_10P  (0x03 << 2)
#define ADC_SE          0x80

/* module parameters */
static unsigned int input_line = 0;
static unsigned int irq = 5;
static int output_line = -1;

MODULE_PARM_DESC(input_line, "Digital input line used for 1-pps");
module_param(input_line, uint, 0);
MODULE_PARM_DESC(irq, "IRQ used for 1-pps");
module_param(irq, uint, 0);
MODULE_PARM_DESC(output_line, "Digital output line to echo 1-pps");
module_param(output_line, int, 0);

/*
 * Convert A/D channel numbers to channel codes. This code is written
 * to bits 4-6 of the A/D command byte. See page 48 of the Iguana
 * manual for the gory details (if you dare :-)
 */
static unsigned int ad_chan_codes[ADC_CHANNELS] = {
    0x00, 0x04, 0x01, 0x05, 0x02, 0x06, 0x03, 0x07
};

struct pps_client_iguana {
    struct pps_device *pps;
    struct tasklet_struct tlet;
    void *spi_membase;
    void *adc_membase;
    unsigned int irq_mask;
    spinlock_t adc_lock;
    struct timeval tv_sample;
    unsigned int samples[ADC_CHANNELS];
#ifdef MONITOR_DIO
    spinlock_t dio_lock;
    int gpio_a;
    int gpio_b;
#endif
};

static struct pps_client_iguana *this_dev = NULL;

#ifdef DEBUG_MODE
static void
dump_spi(void *membase)
{
    int i, ctl, stat, data[4];

    ctl = ioread8(membase + SPI_CONTROL);
    stat = ioread8(membase + SPI_STATUS);
    for(i = 0;i < 4;i++)
        data[i] = ioread8(membase + SPI_DATA0 + i);
    pr_info("SPI regs: %02x/%02x %02x/%02x/%02x/%02x\n",
            ctl, stat, data[0], data[1], data[2], data[3]);
}
#endif

/*
 * Wait for SPI transfer to complete. Returns 0 on success
 * or -ETIMEDOUT on timeout.
 */
static int
spi_wait(void *membase)
{
    int timeout = 10;

    while(ioread8(membase + SPI_STATUS) & SPI_BUSY)
    {
        rmb();
        udelay(5);
        if(timeout-- < 0)
            return -ETIMEDOUT;
    }

    return 0;
}

/*
 * Wait for A/D conversion to complete. Returns 0 on success
 * or -ETIMEDOUT on timeout.
 */
static int
adc_wait(void *membase)
{
    int timeout = 10;

    while(ioread8(membase) & ADC_BUSY)
    {
        rmb();
        udelay(5);
        if(timeout-- < 0)
            return -ETIMEDOUT;
    }

    return 0;
}

/*
 * Write to a register on the MCP23X17 I/O chip
 */
static int
write_io_register(void *membase, unsigned int reg, unsigned int value)
{
    iowrite8(value, membase + SPI_DATA1);
    iowrite8(reg, membase + SPI_DATA2);
    wmb();
    iowrite8(SPI_WRITE_SLAVE, membase + SPI_DATA3);
    wmb();
    return spi_wait(membase);
}

/*
 * Read a register on the MCP23X17 I/O chip
 */
static int
read_io_register(void *membase, unsigned int reg)
{
    int rval;

    iowrite8(0, membase + SPI_DATA1);
    iowrite8(reg, membase + SPI_DATA2);
    wmb();
    iowrite8(SPI_READ_SLAVE, membase + SPI_DATA3);
    wmb();
    rval = spi_wait(membase);

    return rval == 0 ? ioread8(membase + SPI_DATA1) : rval;
}

static int
set_io_register(void *membase, unsigned int reg, unsigned mask)
{
    unsigned int regval;

    regval = read_io_register(membase, reg);
    return write_io_register(membase, reg, regval | mask);
}

static int
clear_io_register(void *membase, unsigned int reg, unsigned mask)
{
    unsigned int regval;

    regval = read_io_register(membase, reg);
    return write_io_register(membase, reg, regval & ~mask);
}

#ifdef MONITOR_DIO
static void
dio_sample(struct pps_client_iguana *dev)
{
    int pa, pb;

    iowrite8(SPI_DIO_CONTROL, dev->spi_membase + SPI_CONTROL);
    iowrite8(SPI_DIO_STATUS | SPI_IRQ_ENABLE | dev->irq_mask,
             dev->spi_membase + SPI_STATUS);
    pa = read_io_register(dev->spi_membase, GPIOA);
    pb = read_io_register(dev->spi_membase, GPIOB);

    spin_lock(&dev->dio_lock);
    dev->gpio_a = pa;
    dev->gpio_b = pb;
    spin_unlock(&dev->dio_lock);
}
#endif

static void
adc_sample_tasklet(unsigned long data)
{
    int chan, i;
    unsigned int ad_val, ad_cmd;
    struct timeval tv;
    unsigned int sample_buf[ADC_CHANNELS];
    struct pps_client_iguana *dev = (struct pps_client_iguana*)data;

    /*
     * Configure SPI bus transactions. We must leave the IRQ enabled
     * so as not to interfere with the 1-PPS.
     */
    iowrite8(SPI_ADC_CONTROL, dev->spi_membase + SPI_CONTROL);
    iowrite8(SPI_ADC_STATUS | SPI_IRQ_ENABLE | dev->irq_mask,
             dev->spi_membase + SPI_STATUS);
    do_gettimeofday(&tv);

    /*
     * Each SPI transaction with the ADC returns the sample from the *previous*
     * conversion, therefore, we need to request ADC_CHANNELS+1 conversions
     * to cycle through all of the channels.
     */
    wmb();
    for(chan = 0,i = -1;chan <= ADC_CHANNELS;chan++,i++)
    {
        ad_cmd = (ad_chan_codes[chan%ADC_CHANNELS] << 4) | ADC_VRANGE_5P | ADC_SE;
        iowrite8(ad_cmd, dev->spi_membase + SPI_DATA3);
        wmb();
        if(spi_wait(dev->spi_membase) < 0)
        {
            pr_err("SPI timeout\n");
            break;
        }

        if(i >= 0)
        {
            /* read the result of the previous conversion */
            ad_val = ioread8(dev->spi_membase + SPI_DATA3);
            sample_buf[i] = (ad_val << 4) | (ioread8(dev->spi_membase + SPI_DATA2) >> 4);
        }
        /* Delay at least 1.5usecs between SPI cycle and A/D conversion */
        udelay(2);
        /* Start the conversion */
        iowrite8(ADC_CONV, dev->adc_membase);
        wmb();
        /* Wait until the converter is ready */
        if(adc_wait(dev->adc_membase) < 0)
        {
            pr_err("ADC timeout\n");
            break;
        }
    }
#ifdef MONITOR_DIO
    dio_sample(dev);
#endif
    /* Copy data to user-accessible buffer */
    spin_lock(&dev->adc_lock);
    memcpy(&dev->samples[0], sample_buf, sizeof(sample_buf));
    dev->tv_sample.tv_sec = tv.tv_sec;
    dev->tv_sample.tv_usec = tv.tv_usec;
    spin_unlock(&dev->adc_lock);
}


static int
check_irq(int irq_num)
{
    int sel_mask = -1;

    switch(irq_num)
    {
        case 3:
            sel_mask = SPI_IRQ_3;
            break;
        case 4:
            sel_mask = SPI_IRQ_4;
            break;
        case 5:
            sel_mask = SPI_IRQ_5;
            break;
        case 10:
            sel_mask = SPI_IRQ_10;
            break;
    }

    return sel_mask;
}

static irqreturn_t
dio_interrupt(int irqnum, void *device)
{
    unsigned int pin, bank;
    int status, mode, intcap_a, intcap_b;
    struct pps_event_time ts_event;
    struct pps_client_iguana *dev = (struct pps_client_iguana*)device;

#if 0
    /* Is this interrupt meant for us? */
    status = ioread8(dev->spi_membase + SPI_STATUS);
    if(!(status & SPI_IRQ_ASSERT))
        return IRQ_NONE;
#endif
    pin = 1 << (input_line & 0x07);
    bank = (input_line >> 3) & 0x01;

    /* Grab a timestamp */
    pps_get_ts(&ts_event);

    /*
     * Clear the interrupt. The i/o chip is configured to mirror interrupts
     * so we must read both INTCAP registers to insure that the interrupt
     * is properly cleared.
     */
    iowrite8(SPI_DIO_CONTROL, dev->spi_membase + SPI_CONTROL);
    iowrite8(SPI_DIO_STATUS | SPI_IRQ_ENABLE | dev->irq_mask,
             dev->spi_membase + SPI_STATUS);
    intcap_a = read_io_register(dev->spi_membase, INTCAPA);
    intcap_b = read_io_register(dev->spi_membase, INTCAPB);
    if(bank)
        mode = (intcap_b & pin) ? PPS_CAPTUREASSERT : PPS_CAPTURECLEAR;
    else
        mode = (intcap_a & pin) ? PPS_CAPTUREASSERT : PPS_CAPTURECLEAR;

    if(output_line >= 0)
    {
        /* Echo the 1-pps to an output line */
        if(mode == PPS_CAPTUREASSERT)
            set_io_register(dev->spi_membase, GPIOA|(output_line >> 3),
                            1 << (output_line & 0x07));
        else
            clear_io_register(dev->spi_membase, GPIOA|(output_line >> 3),
                              1 << (output_line & 0x07));
    }

    /* Schedule the A/D sample tasklet */
    if(mode == PPS_CAPTUREASSERT)
    {
        tasklet_init(&dev->tlet, adc_sample_tasklet, (unsigned long)dev);
        tasklet_schedule(&dev->tlet);
        /* Fire a PPS event */
        pps_event(dev->pps, &ts_event, mode, NULL);
    }

    return IRQ_HANDLED;
}

/*
 * /proc interface to read the most recent A/D sample.
 */
static int
adc_read_last(char *buf, char **start, off_t offset, int len, int *eof, void *data)
{
    char *buf2 = buf;
    int i;
    struct timeval tv;
    unsigned int sample_buf[ADC_CHANNELS];
    struct pps_client_iguana *dev = (struct pps_client_iguana*)data;

    spin_lock(&dev->adc_lock);
    memcpy(sample_buf, &dev->samples[0], sizeof(sample_buf));
    tv.tv_sec = dev->tv_sample.tv_sec;
    tv.tv_usec = dev->tv_sample.tv_usec;
    spin_unlock(&dev->adc_lock);

    buf2 += sprintf(buf2, "%ld.%06ld", tv.tv_sec, tv.tv_usec);
    for(i = 0;i < ADC_CHANNELS;i++)
        buf2 += sprintf(buf2, " %d", sample_buf[i]);
    buf2 += sprintf(buf2, "\n");
    *eof = 1;
    return buf2 - buf;
}

/*
 * /proc interface to read the most recent DIO sample.
 */
#ifdef MONITOR_DIO
static int
dio_read_last(char *buf, char **start, off_t offset, int len, int *eof, void *data)
{
    char *buf2 = buf;
    int i;
    int pa, pb;
    struct pps_client_iguana *dev = (struct pps_client_iguana*)data;

    spin_lock(&dev->dio_lock);
    pa = dev->gpio_a;
    pb = dev->gpio_b;
    spin_unlock(&dev->dio_lock);

    buf2 += sprintf(buf2, "0x%02x 0x%02x\n", pa, pb);
    *eof = 1;
    return buf2 - buf;
}
#endif

static int
enable_io_irq(struct pps_client_iguana *dev, unsigned int line, int o_line)
{
    unsigned int pin = (1 << (line & 0x07)), bank = 0;

    bank = (line > 7) ? 1 : 0;

    iowrite8(SPI_DIO_CONTROL, dev->spi_membase + SPI_CONTROL);
    iowrite8(SPI_DIO_STATUS | SPI_IRQ_ENABLE | dev->irq_mask,
             dev->spi_membase + SPI_STATUS);

    /* Initialize output line */
    if(o_line >= 0)
        clear_io_register(dev->spi_membase, IODIRA|(o_line >> 3),
                          1 << (o_line & 0x07));

    /* Mirror INTA/INTB, open-drain */
    if(write_io_register(dev->spi_membase, IOCONA|bank, 0x44) < 0)
        goto err;

    /* Configure the line as an input */
    if(set_io_register(dev->spi_membase, IODIRA|bank, pin) < 0)
        goto err;

    /* Explicitly disable the input pull-up */
    if(clear_io_register(dev->spi_membase, GPPUA|bank, pin) < 0)
        goto err;

    /* Enable interrupt on state change */
    if(clear_io_register(dev->spi_membase, INTCONA|bank, pin) < 0)
        goto err;

    /*
     * Clear any pending interrupt. We must read both INTCON registers
     * because the interrupts are mirrored.
     */
    read_io_register(dev->spi_membase, INTCAPA);
    read_io_register(dev->spi_membase, INTCAPB);

    if(set_io_register(dev->spi_membase, GPINTENA|bank, pin) < 0)
        goto err;

    return 0;
err:
    return -ETIMEDOUT;
}

static int
disable_io_irq(struct pps_client_iguana *dev, unsigned int line)
{
    unsigned int pin = (1 << (line & 0x07)), bank = 0;

    bank = (line > 7) ? 1 : 0;
    clear_io_register(dev->spi_membase, GPINTENA|bank, pin);

    /* Clear any pending interrupt */
    read_io_register(dev->spi_membase, INTCAPA);
    read_io_register(dev->spi_membase, INTCAPB);

    return 0;
}

static int __init
pps_iguana_init(void)
{
    int result;
    struct pps_source_info info = {
        .name = KBUILD_MODNAME,
        .path = "",
        .mode = PPS_CAPTUREBOTH | \
        PPS_OFFSETASSERT | \
        PPS_OFFSETCLEAR | \
        PPS_ECHOASSERT | PPS_ECHOCLEAR | \
        PPS_CANWAIT | PPS_TSFMT_TSPEC,
        .owner = THIS_MODULE,
        .dev = NULL
    };

    pr_info(DRVDESC "\n");
    this_dev = (struct pps_client_iguana*)kzalloc(sizeof(struct pps_client_iguana), GFP_KERNEL);
    if(!this_dev)
    {
        pr_err("memory allocation failed\n");
        return -ENOMEM;
    }

    this_dev->irq_mask = check_irq(irq);
    if(this_dev->irq_mask == -1)
    {
        kfree(this_dev);
        pr_err("invalid IRQ for DIO PPS (%d)\n", irq);
        return -EINVAL;
    }

    result = request_irq(irq, dio_interrupt, 0, "pps_iguana", this_dev);
    if(result)
    {
        kfree(this_dev);
        pr_err("cannot assign requested IRQ (%d)\n", irq);
        return -EINVAL;
    }

    if(!request_region(SPIX_BASE, SPIX_NREGS, "pps_iguana"))
    {
        free_irq(irq, this_dev);
        kfree(this_dev);
        pr_err("cannot access SPI register address\n");
        return -ENODEV;
    }

    if(!request_region(ADC_CTL_REG, ADC_NREGS, "pps_iguana"))
    {
        release_region(SPIX_BASE, SPIX_NREGS);
        free_irq(irq, this_dev);
        kfree(this_dev);
        pr_err("cannot access SPI register address\n");
        return -ENODEV;
    }

    this_dev->pps = pps_register_source(&info,
                                        PPS_CAPTUREBOTH | PPS_OFFSETASSERT | PPS_OFFSETCLEAR);
    if(!this_dev->pps)
    {
        release_region(ADC_CTL_REG, ADC_NREGS);
        release_region(SPIX_BASE, SPIX_NREGS);
        free_irq(irq, this_dev);
        kfree(this_dev);
        pr_err("failed to register DIO %d as PPS source\n", input_line);
        return -EINVAL;
    }

    this_dev->spi_membase = ioport_map(SPIX_BASE, SPIX_NREGS);
    this_dev->adc_membase = ioport_map(ADC_CTL_REG, ADC_NREGS);

    create_proc_read_entry("iguana_adc", 0, NULL, adc_read_last, this_dev);
#ifdef MONITOR_DIO
    create_proc_read_entry("iguana_dio", 0, NULL, dio_read_last, this_dev);
#endif

    if(enable_io_irq(this_dev, input_line, output_line) < 0)
    {
        remove_proc_entry("iguana_adc", NULL);
#ifdef MONITOR_DIO
        remove_proc_entry("iguana_dio", NULL);
#endif
        pps_unregister_source(this_dev->pps);
        ioport_unmap(this_dev->spi_membase);
        ioport_unmap(this_dev->adc_membase);
        release_region(ADC_CTL_REG, ADC_NREGS);
        release_region(SPIX_BASE, SPIX_NREGS);
        free_irq(irq, this_dev);
        kfree(this_dev);
        pr_err("Failed to configure DIO interrupt\n");
        return -EINVAL;
    }

    pr_info("registered DIO %d (IRQ %d) as PPS source\n", input_line, irq);

    return 0;
}

static void __exit
pps_iguana_exit(void)
{
    disable_io_irq(this_dev, input_line);
    remove_proc_entry("iguana_adc", NULL);
#ifdef MONITOR_DIO
    remove_proc_entry("iguana_dio", NULL);
#endif
    pps_unregister_source(this_dev->pps);
    ioport_unmap(this_dev->spi_membase);
    ioport_unmap(this_dev->adc_membase);
    release_region(ADC_CTL_REG, ADC_NREGS);
    release_region(SPIX_BASE, SPIX_NREGS);
    free_irq(irq, this_dev);
    kfree(this_dev);
    pr_info("removed DIO %d (IRQ %d) as PPS source\n", input_line, irq);
}

module_init(pps_iguana_init);
module_exit(pps_iguana_exit);
MODULE_AUTHOR("Michael Kenney <mikek@apl.uw.edu>");
MODULE_LICENSE("GPL");
